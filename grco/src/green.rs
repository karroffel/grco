use std::collections::VecDeque;
use std::sync::RwLock;

use crate::register_state;
use crate::register_state::RegisterState;

use slab::Slab;

use lazy_static::lazy_static;

// P = Logical Processor
// R = Rust Routine

/// Initial size for the stack of a RustRoutine
const INITIAL_STACK_SIZE: usize = 1024 * 1024 * 2;

/// Alignment of the stack. 8 should be enough for most platforms.
const STACK_ALIGNMENT: usize = 8;

struct StackPointer(*mut u8);

// The stack pointer won't ever be touched concurrently so we know it's safe to use across threads
unsafe impl Send for StackPointer {}
// Same as for `Send` impl
unsafe impl Sync for StackPointer {}

struct EventId(u16);

#[derive(Copy, Clone, Debug)]
struct RustRoutineId(usize);

enum RustRoutineState {
    Running,
    Runnable,
    Waiting(EventId),
}

struct RustRoutine {
    stack: StackPointer,
    state: RustRoutineState,
    registers: RegisterState,
    id: RustRoutineId,
}



struct LogicalProcessor {
    queue: VecDeque<RustRoutineId>,
}

impl LogicalProcessor {
    fn new() -> Self {
        LogicalProcessor {
            queue: VecDeque::new(),
        }
    }

    fn abort_and_next(&self) {
        // TODO
        unimplemented!()
    }
}

struct Storage {
    routines: RwLock<Slab<RustRoutine>>,
    runnables: RwLock<VecDeque<RustRoutineId>>,
}

impl Storage {
    fn new() -> Self {
        Storage {
            routines: RwLock::new(Slab::new()),
            runnables: RwLock::new(VecDeque::new()),
        }
    }

    fn create_routine(&self, mut regs: RegisterState) -> (RustRoutineId, RegisterState) {

        // create stack first
        let (ptr, rsp) = {
            let stack_ptr = {
                use std::alloc::{alloc, Layout};

                let layout = Layout::from_size_align(INITIAL_STACK_SIZE, STACK_ALIGNMENT)
                    .expect("Malformed stack allocation **layout**");

                unsafe { alloc(layout) }
            };

            let stack_begin = stack_ptr as u64;
            let stack_end = stack_begin + (INITIAL_STACK_SIZE as u64);

            (StackPointer(stack_ptr), stack_end)
        };

        // The rsp has to point at the **END** of the stack since the stack grows **down**
        let mut registers = regs;
        registers.rsp = rsp;

        // Then create the routine and insert into the table
        let routine_id = {

            let mut routines = self.routines
                .write()
                .expect("RoutineStorage poisoned");

            let entry = routines.vacant_entry();
            let id = RustRoutineId(entry.key());

            let routine = RustRoutine {
                stack: ptr,
                state: RustRoutineState::Running,
                registers,
                id,
            };

            entry.insert(routine);

            id
        };

        (routine_id, registers)
    }

    fn queue_runnable(&self, rout: RustRoutineId) {
        // TODO
        unimplemented!()
    }
}




lazy_static! {
    static ref STORAGE: Storage = {
        Storage::new()
    };
}

thread_local! {
    static LPROC: std::cell::UnsafeCell<LogicalProcessor> = {
        std::cell::UnsafeCell::new(LogicalProcessor::new())
    };
}


#[inline(never)]
fn spawn_raw<T, F>(f: F)
where
    T: Send + 'static,
    F: Send + 'static,
    F: FnOnce() -> T,
{
    let mut state = register_state::save();

    let (id, regs) = STORAGE.create_routine(state);

    unsafe { register_state::restore(regs) };

    // we start to execute our function, which in all but one case should always contain a
    // yield as the very first thing.
    // This is done because saving the instruction pointer is a pain in the butttt, so we can't just
    // easily save it in a queue or the routine state.
    //
    // Instead, we use the property of return addresses being stored on the stack.
    // If the `yield_now()` function is called from a routine, then the address for the next
    // instruction after that call will be on the call stack.
    //
    // So if we call the yield_now the routine will be queued for later running, but we don't need
    // to store the instruction pointer since the next return will bring us back to where we were
    //
    // NOTE This is just the idea behind it, I have yet to make this work properly OvO'

    let func_with_exit = || {
        f();
        LPROC.with(|proc| {
            let proc = unsafe { &mut *proc.get() };
            proc.abort_and_next();
        });
    };

    func_with_exit();
}

pub fn spawn<T, F>(f: F)
where
    T: Send + 'static,
    F: Send + 'static,
    F: FnOnce() -> T,
{
    // spawn_raw immediately executes the function, so the first thing we do is yield to put it
    // in the runnable queue.
    spawn_raw(|| {
        yield_now();
        f();
    });
}

pub fn spawn_main<T, F>(f: F)
where
    T: Send + 'static,
    F: Send + 'static,
    F: FnOnce() -> T,
{
    // Unlike `spawn`, here we don't yield, since this is the special case
    // of the "root" rust-routine which should be executed immediately
    spawn_raw(|| {
        f();
    })
}

#[inline(never)]
pub fn yield_now() {
    unimplemented!()
}

pub fn current() -> usize {
    unimplemented!()
}

