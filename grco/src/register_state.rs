#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct RegisterState {
    pub(crate) rsp: u64,
    pub(crate) r12: u64,
    pub(crate) r13: u64,
    pub(crate) r14: u64,
    pub(crate) r15: u64,
    pub(crate) rbx: u64,
    pub(crate) rbp: u64,
}

#[inline(always)]
pub fn save() -> RegisterState {
    let mut state = RegisterState {
        rsp: 0,
        r12: 0,
        r13: 0,
        r14: 0,
        r15: 0,
        rbx: 0,
        rbp: 0,
    };

    unsafe {
        asm!("
             movq %rsp, 0(%rdx)
             movq %r12, 8(%rdx)
             movq %r13, 16(%rdx)
             movq %r14, 24(%rdx)
             movq %r15, 32(%rdx)
             movq %rbx, 40(%rdx)
             movq %rbp, 48(%rdx)
            "
            // outputs
            :
            // inputs
            : "{rdx}"(&mut state.rsp)
            // clobbers
            :
            // options
            :
        );
    }

    state
}

#[inline(always)]
pub unsafe fn restore(state: RegisterState) {
    asm!("
         movq 0(%rdx), %rsp
         movq 8(%rdx), %r12
         movq 16(%rdx), %r13
         movq 24(%rdx), %r14
         movq 32(%rdx), %r15
         movq 40(%rdx), %rbx
         movq 48(%rdx), %rbp
        "
        // outputs
        :
        // inputs
        : "{rdx}"(&state.rsp)
        // clobbers
        :
        // options
        :
    )
}


