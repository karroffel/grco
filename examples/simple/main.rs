use grco::green;

fn main() {
    green::spawn_main(green_main);
}

fn green_main() {

    println!("Hello, would you like some PANIC? :)");

    println!("main is {}", green::current());

    green::spawn(test);
    green::spawn(test);
}

fn test() {
    let x = 5;
    let y = &x;

    println!("Helloooo from RustRoutine {}!", green::current());

    println!("{}", *y);

    green::yield_now();

    println!("Bye from RustRoutine {}!", green::current());
}
